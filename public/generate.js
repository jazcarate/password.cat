var basicTimeline = anime.timeline({
    autoplay: false
});

var pathEls = document.getElementsByClassName('check');
for (var i = 0; i < pathEls.length; i++) {
    var pathEl = pathEls[i];
    var offset = anime.setDashoffset(pathEl);
    pathEl.setAttribute("stroke-dashoffset", offset);
}

basicTimeline
    .add({
        targets: ".text",
        duration: 1,
        opacity: "0"
    })
    .add({
        targets: ".button",
        duration: 1300,
        height: 10,
        width: 300,
        backgroundColor: "#2B2D2F",
        border: "0",
        borderRadius: 100
    })
    .add({
        targets: ".button",
        duration: 2000,
        width: 10,
        easing: "linear",
    })
    .add({
        targets: ".button",
        opacity: "1",
        width: 80,
        height: 80,
        delay: 200,
        duration: 750,
        borderRadius: 80,
        backgroundColor: "#4CA1AF"
    })
    .add({
        targets: pathEl,
        strokeDashoffset: [offset, 0],
        duration: 200,
        easing: "easeInOutSine"
    })
    .add({
        targets: pathEl,
        opacity: 0,
        duration: 200,
        delay: 200,
        easing: "easeInOutSine"
    })
    .add({
        targets: ".button",
        delay: 200,
        width: 300,
        duration: 750,
    })
    .add({
        targets: '#response',
        opacity: 1,
        duration: 200,
        easing: "linear",
    });

let generated = false;
document.getElementById('generate').addEventListener('click', function () {
    if (!generated) {
        generated = true;
        cat();
        basicTimeline.play();
    }
});


// What did you expect?
function cat() {
    console.log("meow");
    document.getElementById("response").innerHTML = Math.random().toString(36).slice(-10);
    try {
        gtag('event', 'generate');
    } catch (err) {
        // we tried
    }
}